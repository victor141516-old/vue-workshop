# Vue Workshop

## Builders

### vue-cli

You can use `vue-cli` to generate the boilerplate for a project. \
But ParcelJS is just a better option.

### ParcelJS

Parcel is more developer friendly and provides faster building

## Part 1

```
$ npm init
$ npm install --save vue parcel-bundler
$ touch index.html
$ mkdir src
$ touch main.js
$ mkdir src/components
$ touch src/components/App.vue
$ parcel index.html
```

 - `index.html` is the entrypoint
 - `main.js` is referenced in `index.html`
 - `App.vue` is referenced in `main.js` (and is where Vue magic begins!)


## Part 2

 - Add `header.vue` component
 - Add `example.vue` component
   - Use `functions` and `mounted`


## Part 3

Let's create a new app with the same steps as _Part 1_.\
It'll be an address book.

Add a Vue component (`contact-card`) and iterate over it on `App.vue`


## Part 4

Add support for adding new people:
 - Add `add-contact` component with form
 - Add Vuex as store


## Part 5

Create a new component for the modal:
 - Create the `modal` component
 - Add event logic


## Part 6

Add the router
Add a detailed view for each contact
