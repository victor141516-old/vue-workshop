const Vue = require('vue').default;
const Vuex = require('vuex');
const VueRouter = require('vue-router').default;
const App = require('./App.vue');

Vue.use(Vuex);
Vue.use(VueRouter);

const store = require('./store/main_store');
const router = require('./router/router');

new Vue({
    el: '#app',
    store: store,
    router: router,
    render: h => h(App)
});
