"use strict";

const Vuex = require('vuex');
const ContactsStore = require('./contacts_store');

module.exports = new Vuex.Store({
    modules: {
        contactsStore: ContactsStore
    }
});
