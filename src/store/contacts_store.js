"use strict";

let previousContacts = JSON.parse(localStorage.getItem('contacts') || '[]');

if (!previousContacts.length) {
    previousContacts = [{
        name: 'Victor',
        phone: '555555555',
    },{
        name: 'Maykel',
        phone: '444444444',
    },{
        name: 'Carlos',
        phone: '333333333',
    }];
}

function saveStorage(name, data) {
    localStorage.setItem(name, JSON.stringify(data));
}

module.exports = {
    namespaced: true,
    state: {
        contactsList: previousContacts
    },
    getters: {
        getContact(state) {
            return id => state.contactsList[id];
        }
    },
    mutations: {
        _addContact(state, contact) {
            state.contactsList.push(contact);
            saveStorage('contacts', state.contactsList);
        },
        _deleteContact(state, contactId) {
            state.contactsList.splice(contactId, 1);
            saveStorage('contacts', state.contactsList);
        },
        _updateContact(state, data) {
            state.contactsList[data.id] = data.contact;
            saveStorage('contacts', state.contactsList);
        }
    },
    actions: {
        addContact(context, contact) {
            context.commit('_addContact', contact);
        },
        deleteContact(context, contactId) {
            context.commit('_deleteContact', contactId);
        },
        updateContact(context, data) {
            context.commit('_updateContact', data);
        }
    }
};
