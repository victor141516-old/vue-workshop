"use strict";

const VueRouter = require('vue-router').default;

const pages = {
    list: require('../pages/list'),
    contact: require('../pages/contact-detail')
};

module.exports = new VueRouter({
    mode: 'history',
    routes: [{
        path: '/',
        name: 'list',
        component: pages.list
    }, {
        path: '/contact/:id',
        name: 'contact',
        component: pages.contact
    }]
});
